# xml2json


## Spring Boot v2 + Reactive Web

The project is generated using start.spring.io for Spring Boot 2.0.3.
xml2json is using Reactive Web to execute HTTP requests in a non-blocking manner.


## Xml2JsonConverter

xml2json defines Xml2JsonConverter interface and implements it using two approaches:

- Xml2JsonConverterPojoImpl - POJO method using XML to POJO unmarshalling and subsequent POJO to JSON marshalling.
- Xml2JsonConverterXPathImpl - XPath method using XSLT for modifying DOM to requested JSON format and using org.json.XML for converting XML to JSON. (Assumption - 'no hardcoded references to either field names or field values in your Java code.' do not apply to XSLT query/command definitions) This implementation is only supplementary.

## Testing using @ContextConfiguration

- Two @TestConfiguration are implemented to validate both approaches using the same JUnit.
- Comparison of response JSON file to a given patients.json is done using zjsonpatch library, which presents the diff information in accordance with RFC 6902 (JSON Patch). (Assumption - assignment assumes that canonical json structures should be equal, rather than binary identical - JSON Patch ignores white spaces and order of values according to JSON spec https://www.json.org/)

## Run 

> mvn clean package

Maven will build the project and run unit tests.


### Issues
- age is not correct in a given patients.json