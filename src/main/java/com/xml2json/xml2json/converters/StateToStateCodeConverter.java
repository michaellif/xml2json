package com.xml2json.xml2json.converters;

import java.util.HashMap;
import java.util.Map;

public class StateToStateCodeConverter {

	enum State {

		AL("Alabama"),

		AK("Alaska"),

		AS("American Samoa"),

		AZ("Arizona"),

		AR("Arkansas"),

		CA("California"),

		CO("Colorado"),

		CT("Connecticut"),

		DE("Delaware"),

		DC("District of Columbia"),

		FM("Federated States of Micronesia"),

		FL("Florida"),

		GA("Georgia"),

		GU("Guam"),

		HI("Hawaii"),

		ID("Idaho"),

		IL("Illinois"),

		IN("Indiana"),

		IA("Iowa"),

		KS("Kansas"),

		KY("Kentucky"),

		LA("Louisiana"),

		ME("Maine"),

		MD("Maryland"),

		MH("Marshall Islands"),

		MA("Massachusetts"),

		MI("Michigan"),

		MN("Minnesota"),

		MS("Mississippi"),

		MO("Missouri"),

		MT("Montana"),

		NE("Nebraska"),

		NV("Nevada"),

		NH("New Hampshire"),

		NJ("New Jersey"),

		NM("New Mexico"),

		NY("New York"),

		NC("North Carolina"),

		ND("North Dakota"),

		MP("Northern Mariana Islands"),

		OH("Ohio"),

		OK("Oklahoma"),

		OR("Oregon"),

		PW("Palau"),

		PA("Pennsylvania"),

		PR("Puerto Rico"),

		RI("Rhode Island"),

		SC("South Carolina"),

		SD("South Dakota"),

		TN("Tennessee"),

		TX("Texas"),

		UT("Utah"),

		VT("Vermont"),

		VI("Virgin Islands"),

		VA("Virginia"),

		WA("Washington"),

		WV("West Virginia"),

		WI("Wisconsin"),

		WY("Wyoming");

		private static final Map<String, String> states = new HashMap<String, String>();

		static {
			for (State state : values()) {
				states.put(state.stateName, state.name());
			}
		}

		private String stateName;

		private State(String stateName) {
			this.stateName = stateName;
		}
	}

	public static String convert(String stateName) {
		return State.states.get(stateName);
	}

}
