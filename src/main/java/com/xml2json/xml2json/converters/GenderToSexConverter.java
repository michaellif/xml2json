package com.xml2json.xml2json.converters;

public class GenderToSexConverter {

	enum Gender {
		m("male"), f("female");

		private String sex;

		private Gender(String sex) {
			this.sex = sex;
		}

		public String getSex() {
			return sex;
		}
	}

	public static String convert(String str) {
		return Gender.valueOf(str).getSex();
	}
}
