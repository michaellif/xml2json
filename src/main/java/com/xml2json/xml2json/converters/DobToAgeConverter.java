package com.xml2json.xml2json.converters;

import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;

public class DobToAgeConverter {

	public static int convert(String dob) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");
		LocalDate bday = LocalDate.parse(dob, formatter);
		return Period.between(bday, LocalDate.now()).getYears();
	}

}
