package com.xml2json.xml2json;

public interface Xml2JsonConverter {

	String convert(String block);

}
