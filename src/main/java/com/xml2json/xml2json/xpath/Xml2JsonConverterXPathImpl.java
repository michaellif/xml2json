package com.xml2json.xml2json.xpath;

import java.io.StringReader;
import java.io.StringWriter;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;

import org.json.XML;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.xml2json.xml2json.Xml2JsonConverter;
import com.xml2json.xml2json.converters.DobToAgeConverter;
import com.xml2json.xml2json.converters.GenderToSexConverter;
import com.xml2json.xml2json.converters.StateToStateCodeConverter;

public class Xml2JsonConverterXPathImpl implements Xml2JsonConverter {

	@Override
	public String convert(String xml) {
		try {
			DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();

			InputSource is = new InputSource();
			is.setCharacterStream(new StringReader(xml));

			Document doc = builder.parse(is);
			XPath xPath = XPathFactory.newInstance().newXPath();

			{ // id --> patientid
				NodeList list = ((NodeList) xPath.compile("/patients/patient/id").evaluate(doc,
						XPathConstants.NODESET));
				for (int i = 0; i < list.getLength(); i++) {
					doc.renameNode(list.item(i), null, "patientid");
				}
			}

			{ // gender --> sex
				NodeList list = ((NodeList) xPath.compile("/patients/patient/gender").evaluate(doc,
						XPathConstants.NODESET));
				for (int i = 0; i < list.getLength(); i++) {
					Node node = list.item(i);
					node.setTextContent(GenderToSexConverter.convert(node.getTextContent()));
					doc.renameNode(node, null, "sex");
				}
			}

			{ // dob --> age
				NodeList list = ((NodeList) xPath.compile("/patients/patient/dob").evaluate(doc,
						XPathConstants.NODESET));
				for (int i = 0; i < list.getLength(); i++) {
					Node node = list.item(i);
					node.setTextContent(DobToAgeConverter.convert(node.getTextContent()) + "");
					doc.renameNode(node, null, "age");
				}
			}

			{ // state --> stateCode
				NodeList list = ((NodeList) xPath.compile("/patients/patient/state").evaluate(doc,
						XPathConstants.NODESET));
				for (int i = 0; i < list.getLength(); i++) {
					Node node = list.item(i);
					node.setTextContent(StateToStateCodeConverter.convert(node.getTextContent()) + "");
				}
			}

			String json = XML.toJSONObject(xml2String(doc)).toString(4);
			return json.substring("{\"patients\": {\"patient\": ".length(), json.length() - 2); // Removing patients/patient and leaving unnamed array
		} catch (Exception e) {
			throw new Error(e);
		}
	}

	private static String xml2String(Node node) throws TransformerException {
		TransformerFactory tf = TransformerFactory.newInstance();
		Transformer transformer = tf.newTransformer();
		StringWriter writer = new StringWriter();
		transformer.transform(new DOMSource(node), new StreamResult(writer));
		return writer.getBuffer().toString();
	}
}
