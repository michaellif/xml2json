package com.xml2json.xml2json.pojo;

import java.io.StringReader;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.xml2json.xml2json.Xml2JsonConverter;
import com.xml2json.xml2json.pojo.domain.Patients;

public class Xml2JsonConverterPojoImpl implements Xml2JsonConverter {

	@Override
	public String convert(String xml) {
		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(Patients.class);
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			StringReader reader = new StringReader(xml);

			Patients patients = (Patients) jaxbUnmarshaller.unmarshal(reader);

			ObjectMapper mapper = new ObjectMapper();
			mapper.enable(SerializationFeature.INDENT_OUTPUT);
			return mapper.writeValueAsString(patients.getPatients().toArray());

		} catch (Exception e) {
			throw new Error(e);
		}
	}

}
