package com.xml2json.xml2json.pojo.domain;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "patients")
public class Patients {
	private List<Patient> patients;

	public List<Patient> getPatients() {
		return patients;
	}

	@XmlElement(name = "patient")
	public void setPatients(List<Patient> patients) {
		this.patients = patients;
	}
}
