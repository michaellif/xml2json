package com.xml2json.xml2json.pojo.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.xml2json.xml2json.converters.DobToAgeConverter;
import com.xml2json.xml2json.converters.GenderToSexConverter;
import com.xml2json.xml2json.converters.StateToStateCodeConverter;

@JsonPropertyOrder({ "patientid", "sex", "state", "name", "age" })
public class Patient {

	@JsonProperty("patientid")
	private long id;

	private String sex;

	private String state;

	private String name;

	private int age;

	@JsonIgnore
	private String gender;

	@JsonIgnore
	private String dob;

	public Patient() {
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String gender) {
		sex = GenderToSexConverter.convert(gender);
	}

	public String getState() {
		return StateToStateCodeConverter.convert(state);
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
		setSex(this.gender);
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
		setAge(DobToAgeConverter.convert(dob));
	}
}
