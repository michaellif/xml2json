package com.xml2json.xml2json;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.server.RequestPredicates;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerResponse;

@Configuration
public class Xml2JsonRauter {

	@Bean
	public RouterFunction<ServerResponse> route(Xml2JsonHandler xml2JsonHandler) {
		return RouterFunctions.route(
				RequestPredicates.POST("/xml2json").and(RequestPredicates.accept(MediaType.APPLICATION_XML)),
				xml2JsonHandler::convert);
	}
}