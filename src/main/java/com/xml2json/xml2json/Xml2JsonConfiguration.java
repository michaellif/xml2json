package com.xml2json.xml2json;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.xml2json.xml2json.pojo.Xml2JsonConverterPojoImpl;

@Configuration
public class Xml2JsonConfiguration {

	@Bean
	public Xml2JsonConverter getXml2JsonConverter() {
		return new Xml2JsonConverterPojoImpl();
	}

}
