package com.xml2json.xml2json;

import javax.inject.Inject;

import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.BodyExtractors;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;

import reactor.core.publisher.Mono;

@Component
public class Xml2JsonHandler {

	@Inject
	private Xml2JsonConverter converter;

	public Mono<ServerResponse> convert(ServerRequest request) {
		Mono<String> body = request.body(BodyExtractors.toMono(String.class));
		String block = body.block();
		return ServerResponse.ok().contentType(MediaType.APPLICATION_JSON)
				.body(BodyInserters.fromObject(converter.convert(block)));
	}
}
