package com.xml2json.xml2json;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.nio.file.Files;
import java.util.stream.Collectors;

import javax.inject.Inject;

import org.junit.Test;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.MediaType;
import org.springframework.test.web.reactive.server.WebTestClient;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.flipkart.zjsonpatch.JsonDiff;

import reactor.core.publisher.Mono;

public abstract class Xml2JsonApplicationTestBase {
	
	@Inject
	private WebTestClient webTestClient;

	@Test
	public void testXml2Json() throws Exception {

		String xml = Files.lines(new ClassPathResource("patients.xml").getFile().toPath())
				.collect(Collectors.joining("\n"));

		String refJson = Files.lines(new ClassPathResource("patients.json").getFile().toPath())
				.collect(Collectors.joining("\n"));

		JsonNode refJsonNode = new ObjectMapper().readTree(refJson);

		webTestClient
				// Create a POST request to test an endpoint
				.post().uri("/xml2json").accept(MediaType.APPLICATION_XML).body(Mono.just(xml), String.class).exchange()
				// test assertions against the response
				.expectStatus().isOk().expectBody(String.class).consumeWith(body -> {
					JsonNode convertedJsonNode;
					try {
						convertedJsonNode = new ObjectMapper().readTree(body.getResponseBody());
					} catch (IOException e) {
						throw new Error(e);
					}
					JsonNode patch = JsonDiff.asJson(refJsonNode, convertedJsonNode);
					System.out.println("body = ");
					System.out.println(body.getResponseBody());
					System.out.println("patch = " + patch);
					assertEquals(0, patch.size());
				});
	}
}
