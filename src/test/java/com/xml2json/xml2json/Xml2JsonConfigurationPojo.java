package com.xml2json.xml2json;

import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;

import com.xml2json.xml2json.pojo.Xml2JsonConverterPojoImpl;

@TestConfiguration
public class Xml2JsonConfigurationPojo {

	@Bean
	public Xml2JsonConverter getXml2JsonConverter() {
		return new Xml2JsonConverterPojoImpl();
	}

}
