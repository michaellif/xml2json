package com.xml2json.xml2json;

import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ContextConfiguration(classes = Xml2JsonConfigurationXPath.class)
public class Xml2JsonApplicationXPathTests extends Xml2JsonApplicationTestBase {

}
