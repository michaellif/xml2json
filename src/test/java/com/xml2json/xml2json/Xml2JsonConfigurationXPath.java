package com.xml2json.xml2json;

import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;

import com.xml2json.xml2json.xpath.Xml2JsonConverterXPathImpl;

@TestConfiguration
public class Xml2JsonConfigurationXPath {

	@Bean
	public Xml2JsonConverter getXml2JsonConverter() {
		return new Xml2JsonConverterXPathImpl();
	}

}
